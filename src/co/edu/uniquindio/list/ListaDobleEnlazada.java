package co.edu.uniquindio.list;

import co.edu.uniquindio.sort.Burbuja;

import java.util.Iterator;

public class ListaDobleEnlazada<T> implements Iterable<NodoEnlaceDoble<T>> {

    private NodoEnlaceDoble<T> cabeza;

    public ListaDobleEnlazada() {

    }

    public ListaDobleEnlazada(NodoEnlaceDoble<T> head) {
        this.add(head);
    }

    public void addAsHead(NodoEnlaceDoble<T> item) {
        if (this.cabeza == null) {
            this.add(item);
        }
        else {
            item.setSiguiente(this.cabeza);
            this.cabeza.setAnterior(item);
            this.cabeza = item;
        }
    }

    public void add(NodoEnlaceDoble<T> item) {
        if (this.cabeza == null) {
            this.cabeza = item;
        }
        else {
            NodoEnlaceDoble<T> ultimo = obtenerUltimo();
            if (ultimo != null) {
                ultimo.setSiguiente(item);
                item.setAnterior(ultimo);
            }
        }
    }

    public NodoEnlaceDoble<T> obtenerUltimo() {
        NodoEnlaceDoble<T> next = this.cabeza;
        while (next != null) {
            if (next.getSiguiente() == null) {
                return next;
            }
            next = next.getSiguiente();
        }
        return null;
    }

    public NodoEnlaceDoble<T> remove(T t) {
        if (this.cabeza == null) {
            return null;
        }
        NodoEnlaceDoble<T> previous = this.cabeza.getAnterior();
        NodoEnlaceDoble<T> current = this.cabeza;
        NodoEnlaceDoble<T> next = this.cabeza.getSiguiente();

        // If the item is the first element.
        if (current.get().equals(t) && previous == null) {
            if (this.cabeza.getSiguiente() != null) {
                ((NodoEnlaceDoble<?>) this.cabeza.getSiguiente()).setAnterior(null);
            }
            this.cabeza = next;
            return current;
        }

        while(next != null) {
            if (current.get().equals(t)) {
                previous.setSiguiente((NodoEnlaceDoble<T>) next);
                if (next != null) {
                    next.setAnterior(previous);
                }
                current.setSiguiente(null);
                current.setAnterior(null);
                return current;
            }
            previous = current;
            current = next;
            next = (NodoEnlaceDoble<T>) next.getSiguiente();
        }

        // If the item is the last element.
        if (current.get().equals(t) && previous != null) {
            previous.setSiguiente(null);
            current.setSiguiente(null);
            current.setAnterior(null);
            return current;
        }

        return null;
    }

    public NodoEnlaceDoble<T> removeFirst() {
        if (this.cabeza != null) {
            NodoEnlaceDoble<T> cabeza = this.cabeza;
            this.cabeza = cabeza.getSiguiente();
            this.cabeza.setAnterior(null);
            cabeza.setSiguiente(null);
            return cabeza;
        }
        return null;
    }

    public ListaDobleEnlazada<T> reverse() {
        ListaDobleEnlazada<T> lista = new ListaDobleEnlazada<>();

        for (Iterator<NodoEnlaceDoble<T>> it = this.iterator(); it.hasNext(); ) {
            NodoEnlaceDoble<T> nodo = it.next();
            lista.addAsHead(new NodoEnlaceDoble<>(nodo.get()));
        }

        return lista;
    }

    public Iterator<NodoEnlaceDoble<T>> iterator() {
        Iterator<NodoEnlaceDoble<T>> it = new Iterator<>() {

            private NodoEnlaceDoble<T> current = cabeza;

            @Override
            public boolean hasNext() {
                if (current != null) {
                    return true;
                }
                return false;
            }

            @Override
            public NodoEnlaceDoble<T> next() {
                NodoEnlaceDoble<T> current = this.current;
                this.current = this.current.getSiguiente();
                return current;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        return it;
    }

    public int size() {
        NodoEnlaceDoble<T> next = this.cabeza;
        int size = 0;
        while (next != null) {
            size ++;
            next = next.getSiguiente();
        }

        return size;
    }

    public NodoEnlaceDoble<T> buscar(int index) {
        NodoEnlaceDoble<T> next = this.cabeza;
        int contador = 0;
        while (next != null) {
            if (contador == index) {
                return next;
            }
            next = next.getSiguiente();
            contador++;
        }

        return null;
    }

    public NodoEnlaceDoble<T> shellSort() {
        ListaDobleEnlazada<T> lista = new ListaDobleEnlazada<>();

        NodoEnlaceDoble<T> next = this.cabeza;
        NodoEnlaceDoble<T> next1 =null;
        int actual, auxiliar;

        NodoEnlaceDoble<T> nodoActual=null;
        NodoEnlaceDoble<String> nodoAuxiliar = new NodoEnlaceDoble<>("pruebas");
        actual = size() / 2;
        while (next != null) {
            int tamanomedio = size()/2;
            NodoEnlaceDoble<T> nodoSize = buscar(tamanomedio);
            next1=next;

            if ((actual- tamanomedio) >tamanomedio) {
                nodoAuxiliar.setAnterior(nodoSize.getAnterior());
                nodoAuxiliar.setSiguiente(nodoSize.getSiguiente());

                nodoSize.setAnterior(next.getAnterior());
                nodoSize.setSiguiente(next.getSiguiente());

                next1.setAnterior(nodoAuxiliar.getAnterior());
                next1.setSiguiente(nodoAuxiliar.getSiguiente());
            }
            next = next.getSiguiente();
            actual++;
        }
        return next;
    }

    public void selectionSort(ListaDobleEnlazada<T> stringLis) {
        for (NodoEnlaceDoble<T> Inicial = this.cabeza; Inicial != null; Inicial = Inicial.getSiguiente()) {

            //System.out.println("valores: " + Inicial.get());
            Integer minimo =(Integer)  Inicial.get();
            for (NodoEnlaceDoble<T> actual = this.cabeza; actual != null; actual = actual.getSiguiente()) {
                //System.out.println("valor Actual: " + actual.get());
                if (minimo > (Integer) actual.get()) {
                    minimo=(Integer) Inicial.get();
                    Inicial.set(actual.get());
                    actual.set((T)  minimo);
                    //	System.out.println("valor menor: " + Inicial.get());
                    break;
                }
            }

            //System.out.println("valores: " + Inicial.get());
        }

    }

    static void bubbleSort(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (arr[j] > arr[j+1])
                {
                    // swap arr[j+1] and arr[j]
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
    }

    public void bubbleSort() {
        for (NodoEnlaceDoble<T> i: this) {
            for (NodoEnlaceDoble<T> j: this) {
                if (i.compareTo(j) < 0) {
                    T t = j.get();
                    j.set(i.get());
                    i.set(t);
                }
            }
        }
    }

}
